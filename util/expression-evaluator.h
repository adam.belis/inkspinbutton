//
// Created by Michael Kowalski on 6/22/24.
//

#ifndef EXPRESSION_EVALUATOR_H
#define EXPRESSION_EVALUATOR_H

namespace Inkscape {
namespace Util {

class EvaluatorQuantity {
public:
    EvaluatorQuantity(double value = 0, unsigned int dimension = 0) : value(value), dimension(dimension) {}

    double value;
    unsigned int dimension;
};

class Unit {};

class ExpressionEvaluator {
public:
    ExpressionEvaluator(const char *string, Unit const *unit = nullptr);

    EvaluatorQuantity evaluate();

private:
    const char* _input;
};

} // Util
} // Inkscape

#endif //EXPRESSION_EVALUATOR_H
