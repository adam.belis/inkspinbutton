//
// Created by Michael Kowalski on 6/22/24.
//

#include "expression-evaluator.h"

#include <string>

namespace Inkscape {
namespace Util {

ExpressionEvaluator::ExpressionEvaluator(const char* string, Unit const* unit) {
    _input = string;
}

EvaluatorQuantity ExpressionEvaluator::evaluate() {
    //
    auto value = std::stod(_input);
    return {value / 2, 0};
}

} // Util
} // Inkscape