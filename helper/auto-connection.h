//
// Created by Michael Kowalski on 6/22/24.
//

#ifndef AUTO_CONNECTION_H
#define AUTO_CONNECTION_H

typedef sigc::scoped_connection auto_connection;

#endif //AUTO_CONNECTION_H
