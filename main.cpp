#include <iostream>
#include <gtkmm.h>
#include "ink-spin-button.h"

class MyWindow : public Gtk::Window {
public:
    MyWindow() {
        set_title("Test App");

        auto adjustment1 = Gtk::Adjustment::create(3.14, 0.0, 100.0, 0.1, 1.0, 0.0);
        _button.set_adjustment(adjustment1);
        _button.set_digits(3);
        _button.set_suffix("px");

        auto grid = new Gtk::Grid();
        set_child(*grid);
        grid->set_margin(20);
        grid->set_row_spacing(4);
        int row = 0;
        grid->attach(*new Gtk::Label("Test of the spin button here."), 0, row++, 2);
        grid->attach(*new Gtk::CheckButton("Test"), 1, row++);
        grid->attach(_button, 1, row++);

        auto adjustment2 = Gtk::Adjustment::create(15.0, 0.0, 100.0, 1.0, 5.0, 0.0);
        _button2.set_adjustment(adjustment2);
        grid->attach(_button2, 1, row++);
        _button.set_halign(Gtk::Align::START);
        _button2.set_halign(Gtk::Align::START);
        // _button2.set_size_request(-1, 22);
        // auto m = _button2.measure(Gtk::Orientation::HORIZONTAL);
        grid->attach(*new Gtk::SpinButton(1, 3), 1, row++);

        _button3.set_halign(Gtk::Align::START);
        grid->attach(_button3, 1, row++);
        _button3.set_has_frame(false);

        grid->attach(*Gtk::make_managed<Gtk::Entry>(), 1, row++);
    }

    Inkscape::UI::Widget::InkSpinButton _button;
    Inkscape::UI::Widget::InkSpinButton _button2;
    Inkscape::UI::Widget::InkSpinButton _button3;
};

int main(int argc, char* argv[]) {
    std::cout << "Hello, World!" << std::endl;
    auto application = Gtk::Application::create("org.tavmjong.spinbutton4");
    return application->make_window_and_run<MyWindow>(argc, argv);
    // return 0;
}
